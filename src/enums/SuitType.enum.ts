export enum SuitType {
  HEART = "HEART",
  DIAMOND = "DIAMOND",
  SPADE = "SPADE",
  CLUB = "CLUB"
}