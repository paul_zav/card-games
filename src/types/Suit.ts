import { Color } from "../enums/Color.enum";
import { SuitType } from "../enums/SuitType.enum";

export interface Suit {
  type: SuitType;
  color: Color;
  trump: boolean;
}