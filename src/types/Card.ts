import { CardType } from "../enums/CardType.enum";
import { Suit } from "./Suit";

export interface Card {
  suit: Suit;
  type: CardType;
  value: number;
  wild: boolean;
}