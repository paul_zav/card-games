import { GameType } from "../enums/GameType.enum";
import { Player } from "./Player";

export interface Game {
  players: Player[];
  type: GameType;
}